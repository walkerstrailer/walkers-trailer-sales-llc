Walker's Trailer Sales LLC


We're a full-service semi-trailer dealership specializing in open deck trailers for owner/operators across the US. We sell and rent new and used trailers and provide parts and service. We have the expertise to help you design your new trailer to meet your specific needs.


Address: 445 Industrial Blvd, La Vergne, TN 37086, USA


Phone: 615-641-6655


Website: https://wtstn.com
